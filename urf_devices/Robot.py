from urf_middleware import Client, Message
from urf_algorithms import VideoFrame, VideoDecoder

import numpy as np

from .Device import Device


class Robot(Device):
    def __init__(self, client_address):
        super().__init__(client_address)

    def set_velocity(self, velocity) -> bool:
        msg = Message()
        msg.header().writer_id(self._writer_id)

        msg["cart_vel"] = velocity
        return self._client.push(msg)

