import logging

from urf_middleware import Message
from urf_algorithms import VideoFrame, VideoDecoder

import numpy as np

from .Device import Device


class Camera(Device):
    def __init__(self, client_address):
        super().__init__(client_address)
        
        self.__logger = logging.getLogger(__name__)
        self.__camera_topic = None
        self.__decoder = None

    def start_stream(self, encoding = "x264", fps = None, width = None, height = None) -> bool:
        msg = Message()
        msg.header().writer_id(self._writer_id)

        msg["cmd"] = "start_stream"
        msg["rgb"] = {"encoding": encoding}
        
        if fps:
            msg["rgb"]["fps"] = fps

        if width:
            msg["rgb"]["width"] = width

        if height:
            msg["rgb"]["height"] = height

        response = self._client.request(msg, 5000)
        if len(response) == 0 or not response[0]["retval"]: 
            self.__logger.error("Failed to start stream")
            return False

        self.__camera_topic = response[0]["topicname"]
        if not self._client.subscribe(self.__camera_topic):
            self.__logger.error("Failed to subscribe to stream")
            return False
        
        self.__decoder = VideoDecoder(encoding)

        return True

    def get_frame(self) -> VideoFrame:
        if self.__decoder is None:
            self.__logger.error("Stream not started")
            return None

        frame = self._client.receive_update(self.__camera_topic, 5000)
        if not frame:
            self.__logger.error("Nothing received")
            return None

        self.__decoder.add_bytes(frame["rgb"])
        decoded = self.__decoder.get_frame()
        if decoded.empty():
            self.__logger.error("Invalid frame")
            return None

        return np.array(decoded)
