import logging

from urf_middleware import Client, Message

TIMEOUT=5000

class Device():
    def __init__(self, client_address):
        self.__logger = logging.getLogger(__name__)
        self.__logger.debug("__init__()")

        self._client = Client(client_address)
        self._writer_id = None

        if not self._client.open():
            self.__logger.error("Could not open client")
            raise Exception("Could not open client")

    def __del__(self):
        self._client.close()

    def __request_change_state(self, state):
        msg = Message()
        msg["cmd"] = state
        msg.header().writer_id(self._writer_id)

        response = self._client.request(msg, TIMEOUT)
        if len(response) == 0:
            return False
        
        if not response[0]["retval"]:
            self.__logger.error(response[0])
            return False
        return True
    
    @property
    def settings(self):
        msg = Message()
        msg["cmd"] = "get"
        msg["node"] = "/"

        response = self._client.request(msg, TIMEOUT)

        return response[0]["data"]
    
    @property
    def server_info(self):
        msg = Message()
        msg["cmd"] = "get"
        msg["node"] = "server"

        response = self._client.request(msg, TIMEOUT)

        return response[0]["data"]
    
    @property
    def state(self):
        msg = Message()
        msg["cmd"] = "get"
        msg["node"] = "component/state"

        response = self._client.request(msg, TIMEOUT)

        return response[0]["data"]["value"]

    def acquire(self, role, username) -> bool:
        msg = Message()
        msg["cmd"] = "acquire"
        msg["role"] = role
        msg["user"] = username

        response = self._client.request(msg, TIMEOUT)

        if len(response) == 0:
            return False
        
        if not response[0]["retval"]:
            return False
        
        self._writer_id = int(response[0]["id"])
        return True

    def release(self) -> bool:
        msg = Message()
        msg["cmd"] = "release"
        response = self._client.request(msg, TIMEOUT)

        if len(response) == 0:
            return False
        
        if not response[0]["retval"]:
            return False
        
        self._writer_id = None
        return True
    
    def enable(self) -> bool:
        return self.__request_change_state("enable")
    
    def disable(self) -> bool:
        return self.__request_change_state("disable")
    
    def reconfigure(self) -> bool:
        return self.__request_change_state("reconfigure")
    
    def shutdown(self) -> bool:
        return self.__request_change_state("shutdown")
    
    def switchon(self) -> bool:
        return self.__request_change_state("switchon")
    
    def reset_fault(self) -> bool:
        return self.__request_change_state("reset")