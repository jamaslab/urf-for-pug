import logging
import cv2
import keyboard

from urf_devices import Camera, Robot

if __name__ == "__main__":
    FORMAT = '%(asctime)s %(name)s %(message)s'
    logging.basicConfig(format=FORMAT)

    camera = Camera("tcp://192.168.0.2:8083")
    camera.acquire("expert", "glunghi")
    camera.enable()

    robot = Robot("tcp://192.168.0.2:8084")
    robot.acquire("expert", "glunghi")
    robot.enable()

    camera.start_stream()
    while True:
        frame = camera.get_frame()

        if not frame is None:
            cv2.imshow("test", frame)
            if cv2.waitKey(1) & 0xFF == ord('q'):
                break

        velocity = [0, 0, 0]

        if keyboard.is_pressed('w'):
            velocity[0] = 0.5
        elif keyboard.is_pressed('s'):
            velocity[0] = -0.5

        if keyboard.is_pressed('d'):
            velocity[1] = 0.5
        elif keyboard.is_pressed('a'):
            velocity[1] = -0.5

        robot.set_velocity(velocity)

    print("Finished")
    cv2.destroyAllWindows()
